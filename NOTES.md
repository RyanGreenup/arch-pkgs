# My Packages

The idea is to write `PKGBUILDS` to have a redistributable system.

to install a package:

    1. Add it to one of these files
    2. build the meta-package
    3. install the meta package


It may, likely, be more convenient to have a local repository,
that way one master meta-package could install all the other
sub-meta-packages (this would also require the packages start
with a unique identifier, like my name). Another advantage to a personal package repository is that I would only need to build Aur packages on one machine and then I could share them with others.


## Building Packages

If the meta-packages contain packages that are in the AUR it will
be necessary to use `paru`:

```bash
paru --skipreview -Ui
```

There may be some extra stuff to do regarding orphans not being owned
by the meta (refer to [the wiki notes](http://localhost/mediawiki/index.php/Arch/Managing_Arch_with_Custom_Packages))

Although I could integrate my own `PKGBUILDS` I may as well leverage
the up-to-date AUR scripts.

## How to sync it?


1. Loop over with `paru -Ui`
   1. This would **only** work with `paru`, not with `pacman`
2. (Optional) Write a  Master package and use a  local repository for a 
   one package install. 

### TODO How can I make this represent the final state of my system

Add a package to these
rebuild all the packages with a makefile
add all the packages to the repo
update the package from the repos.

When uninstalling packages make sure to remove all orphans, if using 
a master package this could be done with:

```bash
# before the package contents are installed when a package is being upgraded
pre_upgrade() { 
    # use -n (--nosave) to remove config files as well,
    # These should be managed by the `PKGBUILD`
    pacman -Rs --nosave $(pacman -Qqtd)
 }
```



### TODO How to handle configs
I think I'll need to uninstall with `pacman -Rsn` the `-n` removing the
config files on the system (`--nobackup`).

### DONE Will AUR packages be owned by the meta-package if installed with `paru`?

I just tested this with `vala-language-server` which is only in the 
AUR:

  1. add `vala-language-server` to a `PKGBUILD`
  2. Install it with `paru -Ui`
  3. look for it in the system with:
    ```bash
    # Not found
    pacman -Qe | grep vala-language-server
    # Found
    pacman -Qe | my-package
    ```


#### TODO How can I handle Aur packages without `paru`?

So yes, the AUR packages will be owned by `paru`, if this was not the case
I could:

  1. `git clone` the `PKGBUILD`
  2. Build the package with `makepkg`
  3. add the package to a local repo
  4. then finally build the desired package with `makepkg` and it will 
     be identified in the local repository.

##### TODO Does this require the order of packages to be monitored?

Yeah, it would matter and I would need to set up some sort of structure
to avoid or manage dependency hell if that happened.

I don't know how I could do that.


## Notes 
### Conflicting nested dependencies
Say i'm installing `my-editors` which has `neovim` and `vscode-html-languageserver`,
let's say that `vscode-html-languageserver` depends on `neovim-git` which would be in
conflict with `neovim`, this will be an issue that `pacman` cannot 
overcome and you would need to adopt `neovim-git` in that case.

### Installing as dependencies
Packages installed voluntarily will be ignored by meta-packages if they
are in the `depends` array, it is necessary to uninstall all packages
from `pacman -Qe` and have them all handled by meta packages for this
to work well, otherwise uninstalling the meta package will not lead
to those packages being seen as orphans and they won't be removed.