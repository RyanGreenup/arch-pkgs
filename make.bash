#!/bin/bash
set -o errexit  # abort on nonzero exitstatus
set -o nounset  # abort on unbound variable
# set -o pipefail # don't hide errors within pipes

BOLD='\e[1;1m'
DIM='\e[2;1m'
EM='\e[3;1m'
UL='\e[4;1m'

RED='\e[1;31m'
GREEN='\e[1;32m'
YELLOW='\e[1;33m'
BLUE='\e[1;34m'
PURPLE='\e[1;35m'
CYAN='\e[1;36m'
RESET='\e[0m'


# .............................................................................
# Note that I've used paru not pacman, this way that package can handle
# the sudo loop and I don't have to think about it.
# Also the advantage to `paru` over `makepkg` is that it will resolve AUR
# dependencies (even in a local PKGBUILD unlike `yay`)
# .............................................................................

# Print Line Function----------------------------------------------------------
line() {
    for i in $(seq 1 80); do echo -n '-'; done; echo ''
}
heading() {
    line
    echo -e "${1}"
    line
}

# Change Directory ------------------------------------------------------------
this_dir="$( cd "$( dirname "$0" )" && pwd )"
cd "${this_dir}"


# Get Package Directories -----------------------------------------------------
pkg_dirs="$(find ./ -name PKGBUILD | xargs dirname | xargs realpath)"

# Loop over Packages ----------------------------------------------------------
for dir in ${pkg_dirs}
do
    # Change into the directory ===============================================
    cd "$(realpath ${dir})";
    heading "${BOLD}${RED} Installing ${RESET} ${dir}"
    # Clean any old builds from that directory ================================
    if [[ -d "pkg" ]]; then rm -rf "pkg"; fi
    if [[ -d "src" ]]; then rm -rf "src"; fi
    find ./ -name '*tar*' -exec rm {} \;
    # Install the package with AUR helper======================================
    paru --skipreview --noconfirm --needed -Ui --cleanafter
    # Switch all dependencies to `--asdepends`=================================
    line
    echo -e "Marked as ${BOLD}${BLUE} Dependent:\n ${RESET} ${depends[@]}"
    source PKGBUILD
    paru -D --asdeps "${depends[@]}" > /dev/null || echo "Dependency Error"
    line
    # Change back to the root directory =======================================
    cd "${this_dir}"

    # Add packages to local repo for other devices ============================
    # repo-add

done

# Remove all Orphans ----------------------------------------------------------
orphans="$(pacman -Qdt)"
if [[ "${orphans}" != "" ]]
then
    echo -e "\nThe following packages are Orphans:\n"
    paru -R --nosave $(pacman -Qqtd)
fi

# List Packages Currently Installed -------------------------------------------
heading "${BLUE}Installed Packages${RESET}"
pacman -Qe
