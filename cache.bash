#!/bin/bash
set -o errexit  # abort on nonzero exitstatus
set -o nounset  # abort on unbound variable
# set -o pipefail # don't hide errors within pipes

BOLD='\e[1;1m'
DIM='\e[2;1m'
EM='\e[3;1m'
UL='\e[4;1m'

RED='\e[1;31m'
GREEN='\e[1;32m'
YELLOW='\e[1;33m'
BLUE='\e[1;34m'
PURPLE='\e[1;35m'
CYAN='\e[1;36m'
RESET='\e[0m'


# .............................................................................
# Note that I've used paru not pacman, this way that package can handle
# the sudo loop and I don't have to think about it.
# Also the advantage to `paru` over `makepkg` is that it will resolve AUR
# dependencies (even in a local PKGBUILD unlike `yay`)
# .............................................................................

# Print Line Function----------------------------------------------------------
line() {
    for i in $(seq 1 80); do echo -n '-'; done; echo ''
}
heading() {
    line
    echo -e "${1}"
    line
}

# Change Directory ------------------------------------------------------------
this_dir="$( cd "$( dirname "$0" )" && pwd )"
cd "${this_dir}"


# Get Package Directories -----------------------------------------------------
pkg_dirs="$(find ./ -name PKGBUILD | xargs dirname | xargs realpath)"

# Pull Dependencies
pkg_list="./all_packages.txt"
pkg_list="$(readlink -f ${pkg_list})"
if [[ -f "${pkg_list}" ]]; then rm "${pkg_list}"; else touch "${pkg_list}";  fi
for dir in ${pkg_dirs}
do
    # Change into the directory ===============================================
    cd "$(realpath ${dir})";
    heading "${BOLD}${RED} Caching ${RESET} ${dir}"
    line
    source PKGBUILD
    echo "${depends[@]}" | tr -d '\n' >> ${pkg_list}
    # Change back to the root directory =======================================
    cd "${this_dir}"
done

echo "Caching All Packages............................................................"
echo ""
echo "Abort with C-c"
sudo powerpill -Sw --needed - < "${pkg_list}"
